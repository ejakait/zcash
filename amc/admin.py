from django.contrib import admin

from .models import FundHouse,FundCategory,FundScheme, AssetPortfolio, SchemePlan
# Register your models here.
@admin.register(FundHouse)
class FundHouseAdmin(admin.ModelAdmin):
    list_display = ('name','amc_code','launch_date','total_aum',)
    fields = list_display
admin.site.register(FundCategory)
@admin.register(FundScheme)
class FundSchemeAdmin(admin.ModelAdmin):
    list_display = ('name' , 'fund_house', 'fund_category',)
    fields = list_display
@admin.register(SchemePlan)
class SchemePlanAdmin(admin.ModelAdmin):
    list_display = ('name', 'fund_scheme', 'min_investment','asset_size','expense_ratio','launch_date',)
    fields = list_display
admin.site.register(AssetPortfolio)
