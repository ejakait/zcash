from .models import SchemePlan
import django_tables2 as tables

class SchemePlanTable(tables.Table):
    class Meta:
        model = SchemePlan