from django.conf.urls import url

from amc import views

urlpatterns = [
    url(r'^dashboard/$', views.DashboardView.as_view(), name='dashboard'),

    url(r'^funds/$', views.SchemePlanList.as_view(), name='scheme_list'),
    url(r'^funds/(?P<scheme_id>[-_\w]+)/details$', views.scheme_detail, 
        name='scheme_detail'),
]
