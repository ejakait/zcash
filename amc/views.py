from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView, TemplateView
from django.shortcuts import get_object_or_404, render

from .models import SchemePlan


@method_decorator(login_required, name='dispatch')
class DashboardView(TemplateView):

    template_name = 'dashboard.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DashboardView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        c = super(DashboardView, self).get_context_data(**kwargs)
        user = self.request.user
        
        return c


class SchemePlanList(ListView):
    model = SchemePlan
    queryset = SchemePlan.objects.all()
    context_object_name = 'scheme'
    template_name = 'scheme_list.html'


def scheme_detail(request,scheme_id):
    scheme = get_object_or_404(SchemePlan, pk=scheme_id)
    return render(request, 'scheme_detail.html', {'scheme': scheme})
