#!/bin/bash

# Start Gunicorn processes
# pip install git+https://github.com/nitely/Spirit.git
python manage.py collectstatic
echo Starting Gunicorn.
exec gunicorn zcash.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 3
