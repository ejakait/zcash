from django.contrib import admin
from .models import Transaction, Portfolio

# Register your models here.


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('user', 'scheme_plan', 'transaction_type', 'status','amount',)
    fields = list_display


@admin.register(Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
    list_display = ('user', 'scheme_plan',)
    fields = list_display
