from django import forms
from django.forms import ModelForm
from .models import Transaction


class AmountForm(ModelForm):
    class Meta:
        model = Transaction
        fields = ['amount','transaction_type']
    # amount = forms.CharField(max_length=6)
    # transaction_type = forms.CharField(
    #     max_length=1)  # purchase redemption etc

    # def clean(self):
    #     cleaned_data = super(AmountForm, self).clean()
    #     amount = cleaned_data.get('amount')
    #     transaction_type = cleaned_data.get('transaction_type')
