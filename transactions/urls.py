from django.conf.urls import url

from transactions import views

urlpatterns = [
    url(r'^checkout/(?P<scheme_id>[-_\w]+)/', views.checkout, name='checkout'),

]
