import stripe
from bigchaindb_driver import BigchainDB
from bigchaindb_driver.crypto import generate_keypair
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.shortcuts import get_object_or_404, redirect, render

from amc.models import SchemePlan
from transactions.models import Portfolio, Transaction
from zcash import bigchain

from .forms import AmountForm

bdb_root_url = 'http://localhost:9984'
bdb = BigchainDB(bdb_root_url)


stripe.api_key = settings.STRIPE_SECRET_KEY


@login_required
def checkout(request, scheme_id):
    publishKey = settings.STRIPE_PUBLISHABLE_KEY
    customer = stripe.Customer.create(
        email=request.user.email,
        source=request.POST.get('stripeToken',),
    )
    scheme = get_object_or_404(SchemePlan, pk=scheme_id)
    scheme_serialize = serializers.serialize(
        "json", SchemePlan.objects.filter(pk=scheme_id))
    if request.method == 'POST':
        form = AmountForm(request.POST)
        if form.is_valid():
            tx_amount = request.POST.get('amount')
            transaction_type = request.POST['transaction_type']

        try:
            charge = stripe.Charge.create(
                customer=customer.id,
                amount=tx_amount*100,
                currency="usd",
                description="Investment Order",
            )
            transaction = Transaction.objects.update_or_create(
                user=request.user, scheme_plan=scheme, transaction_type='Purchase', status='Requested Internally', amount=tx_amount)
            request.user = generate_keypair()
            tx_data = {
                "data": {
                    "scheme_transaction": {
                        "scheme_plan": scheme_serialize,
                        "amount": charge["amount"],
                    }
                }
            }
            prepared_creation_tx = bdb.transactions.prepare(
                operation='CREATE',
                signers=request.user.public_key,
                asset=tx_data,
            )
            fulfilled_creation_tx = bdb.transactions.fulfill(
                prepared_creation_tx, private_keys=request.user.private_key
            )
            print(fulfilled_creation_tx)
            sent_creation_tx = bdb.transactions.send(fulfilled_creation_tx)
            print(sent_creation_tx)

            def award(self, request):
                user = request.user.award_points(5000)

            return redirect('amc:scheme_list')

        except stripe.error.CardError as e:
            print(e)
    else:
        form = AmountForm()
    context = {"publishKey": publishKey, "scheme": scheme, "form": form}
    template = 'checkout.html'
    return render(request, template, context)
