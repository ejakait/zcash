from django.contrib import admin
from .models import User
from django.contrib import auth

# from rest_framework.authtoken.models import Token


# Register your models here.
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email','national_id','phone',)
    fields = ('is_active','is_staff',)
admin.site.unregister(auth.models.Group)
# admin.site.unregister(Token)
