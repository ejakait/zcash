from django import forms
from registration.forms import RegistrationForm
from django.contrib.auth.forms import UserChangeForm


from users.models import User


class CustomUserChangeForm(UserChangeForm):

    class Meta(UserChangeForm.Meta):
        model = User


class CustomUserCreationForm(RegistrationForm):
    email = forms.EmailField(
        max_length=254, help_text='Required. Inform a valid email address.')

    # http://james.lin.net.nz/2013/06/08/django-custom-user-model-in-admin-relation-auth_user-does-not-exist/
    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')

        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError(
                ("This email address is already in use. Please supply a different email address."))
        return email

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )
