from __future__ import unicode_literals

import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils.encoding import python_2_unicode_compatible
# from django.utils.translation import ugettext_lazy as _
from bigchaindb_driver.crypto import generate_keypair
from django.db.models.signals import post_save
from django.contrib.auth.signals import user_logged_in
from django.db import models


class User(AbstractUser):
    national_id = models.CharField(max_length=15, blank=True, null=False)
    phone = models.CharField(max_length=12, default='')
    email = models.EmailField()
    points = models.IntegerField(default=0)

    def award_points(self, amount):
        self.amount = amount
        self.points += amount
        self.save()

    def __str__(self):
        return self.username


def create_keypair(sender, instance, created, **kwargs):
    if created:
        user = instance
        user = generate_keypair()


post_save.connect(create_keypair, sender=User)


def award_points_on_login(sender, user, **kwargs):
    user = user.award_points(5000)


user_logged_in.connect(award_points_on_login)
