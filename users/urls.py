from django.conf.urls import url

from users import views

urlpatterns = [
    url(r'^profile/update/(?P<pk>[-\w]+)/$',
        views.UserUpdate.as_view(), name='profile'),
    url(r'^profile/portfolio/',
        views.portfolio, name='portfolio'),

]
