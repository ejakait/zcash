from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, UpdateView, View

from .models import User
from transactions.models import Transaction

# Create your views here.


class IndexView(TemplateView):
    """
    View pointing to Landing
    """
    template_name = 'landing.html'


@method_decorator(login_required, name='dispatch')
class UserUpdate(UpdateView):
    model = User
    fields = ['username', 'first_name', 'last_name',
              'email', 'phone', 'national_id', 'password', ]
    template_name = 'user_update_form.html'

    def get_success_url(self):
        return reverse('dashboard')
    
    def award(self,request):
        user = request.user.award_points(5000)



    def get_object_or_404(self):
        return User.objects.get(pk=self.request.GET.get('pk'))

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdate, self).dispatch(*args, **kwargs)

@login_required
def portfolio(request):
    transactions = Transaction.objects.filter(user = request.user)
    return render(request, 'portfolio.html',{"transactions":transactions})
