"""zcash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from registration.backends.hmac.views import RegistrationView
from users import views
from django.utils.translation import ugettext_lazy as _


from users.forms import CustomUserCreationForm
from transactions import views as checkout_views

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    url(r'^jet/dashboard/', include('jet.dashboard.urls',
                                    'jet-dashboard')),  # Django JET dashboard URLS
    url(r'^admin/', admin.site.urls),

    url(r'^amc/', include('amc.urls', namespace="amc")),
    url(r'^users/', include('users.urls', namespace="users")),

    url(r'^transactions/', include('transactions.urls')),
    url(r'^forum/', include('spirit.urls')),

    # url(r'^dashboard/', views.HomeView.as_view(), name='dashboard'),
    url(r'^$', views.IndexView.as_view(), name='index'),


    url(r'^accounts/register/$',
        RegistrationView.as_view(
            form_class=CustomUserCreationForm
        ),
        name='registration_register',
        ),
    url(r'^accounts/', include('registration.backends.hmac.urls')),

]

admin.site.site_header = _("Z-CASH Admin")
admin.site.site_title = _("Z-CASH Admin")
